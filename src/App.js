import React from 'react';
import './App.css';
import Register from './Register.js';

function App() {
    return (
        <div className="App">
            <Register/>
        </div>
    );
}

export default App;
