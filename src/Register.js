import React from 'react';
import RegistrationList from "./RegistrationList";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';


class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {names: [], firstName: '', lastName: '', middleName: ''};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    render() {
        const title = 'Register Here';
        return (
            <Container>
                <Row>
                    <h3>{title}</h3>
                </Row>
                <Row>
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <input
                                className="form-control"
                                id=""
                                name="firstName"
                                placeholder="Enter first name.."
                                onChange={this.handleChange.bind(this)}
                                value={this.state.firstName}
                            />
                        </div>
                        <div className="form-group">
                            <input
                                className="form-control"
                                id=""
                                name="middleName"
                                placeholder="Enter middle name.."
                                onChange={this.handleChange.bind(this)}
                                value={this.state.middleName}
                            />
                        </div>
                        <div className="form-group">
                            <input
                                className="form-control"
                                id=""
                                name="lastName"
                                placeholder="Enter Last name.."
                                onChange={this.handleChange.bind(this)}
                                value={this.state.lastName}
                            />
                        </div>
                        <div className="form-group">
                            <Button variant="primary" size="sm" onClick={this.handleSubmit.bind(this)}>Add</Button>
                        </div>
                    </form>
                </Row>
                <Row>
                    <RegistrationList names={this.state.names} handleDelete={this.handleDelete}/>
                </Row>
            </Container>
        );
    }

    handleDelete(id) {
        for (var i = 0; i < this.state.names.length; i++) {
            if (this.state.names[i].id === id) {
                this.state.names.splice(i, 1);
            }
        }
        this.setState(state => ({
            names: state.names,
            firstName: '',
            middleName: '',
            lastName: ''
        }));

    }

    handleChange({target}) {
        const value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [target.name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.firstName.length) {
            return;
        }
        if (!this.state.lastName.length) {
            return;
        }

        const newItem = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            middleName: this.state.middleName,
            id: Math.random()
        };
        this.setState(state => ({
            names: state.names.concat(newItem),
            firstName: '',
            middleName: '',
            lastName: ''
        }));
    }
}

export default Register;