import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";

class RegistrationList extends React.Component {
    handleDelete(id) {
        this.props.handleDelete(id);
    }

    render() {
        return (
            <Container><Row>
                <h3>Registered Dashboard</h3>
            </Row>
            <ListGroup variant="flush">
                {this.props.names.map(name => (
                    <ListGroup.Item
                        key={name.id}>{name.firstName} &nbsp; {name.middleName} &nbsp; {name.lastName} &nbsp;
                        <Button variant="danger" size="sm"
                                onClick={this.handleDelete.bind(this, name.id)}>Delete</Button>
                    </ListGroup.Item>
                ))}
            </ListGroup>
            </Container>
        );
    }
}

export default RegistrationList;